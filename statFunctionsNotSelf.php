<?php

/**
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2022 Denis Chenu
 * @license GPL version 3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
class statFunctionsNotSelf extends PluginBase
{
    protected static $description = 'Add some function in ExpressionScript Engine to get count from other responses';
    protected static $name = 'statFunctionsNotSelf';

    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    public function init()
    {
        $this->subscribe('beforeActivate');
        $this->subscribe('ExpressionManagerStart', 'newValidFunctions');
    }

    public function beforeActivate() {
        $event = $this->getEvent();
        $oPluginStatFunctions = \Plugin::model()->find(
            "name = :name",
            array(":name" => 'statFunctions'),
        );
        if(!$oPluginStatFunctions || !$oPluginStatFunctions->active) {
            $event->set('success', false);
            $event->set('message', sprintf($this->gT('You must activate %s plugin'), 'statFunctions'));
        }
    }

    public function newValidFunctions()
    {
        if (empty(Yii::getPathOfAlias('statFunctions'))) {
            $this->log('error', "You must activate statFunctions plugin for statFunctionsNotSelf");
            die("toto");
            return;
        }
        Yii::setPathOfAlias(get_class($this), dirname(__FILE__));
        $newFunctions = array(
            'statCountIfNotSelf' => array(
                '\statFunctionsNotSelf\countFunctionsNotSelf::statCountIfNotSelf',
                null, // No javascript function : set as static function
                $this->gT("Count the number of complete responses  with a value equal to a specific value"), // Description for admin
                'integer statCountIfNotSelf(QuestionCode.sgqa, value[, submitted = true])', // Extra description
                'https://support.sondages.pro', // Help url
                2, // Number of argument unsure it work here … , minimum 2, allow 3
                3
            ),
            'statCountNotSelf' => array(
                '\statFunctionsNotSelf\countFunctionsNotSelf::statCountNotSelf',
                null, // No javascript function : set as static function
                $this->gT("Count the number of complete responses which are not empty"), // Description for admin
                'integer statCountNotSelf(QuestionCode.sgqa[, submitted = true])', // Extra description
                'https://support.sondages.pro', // Help url
                1, // Number of argument (time to make a good description of EM …) minimum 1, allow 2
                2,
            ),
        );
        $this->getEvent()->append('functions', $newFunctions);
    }
}
