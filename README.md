# statFunctionsNotSelf

Same than core function for expression manager to count some statictics data except current reponse (adding id != {SAVEDID})

This plugin can be replaced by core statFunctions since LimeSurvey 5.3.29.

## Usage

Same than core function for expression manager to count some statictics data except current reponse (adding id != {SAVEDID})

- `statCountNotSelf(QuestionCode.sgqa[, submitted = true])` Count the number of responses which are not empty
- `statCountIfNotSelf(QuestionCode.sgqa, value[, submitted = true])` Count the number of complete responses  with a value equal to a specific value

Some example `statCountIfNotSelf(QuestionCode.sgqa, QuestionCode.NAOK, 0)` allow unicity of a value in whole data (submitted ot not).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2022 Denis Chenu / SondagesPro <https://sondages.pro>
- [Support](https://support.sondages.pro)
- [Donate](https://support.sondages.pro/open.php?topicId=12)

Distributed under [GNU GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/gpl.txt) licence
